<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
      public function create() {
        return view('pertanyaan.form');
    }

    public function store(Request $request) {
        // dd($request->all());
        $request->validate([
            "judul" => 'required',
            "isi" => 'required',
        ]);

        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);
        return redirect('/pertanyaan')->with('success', 'Post berhasil disimpan!');
    }

    public function index() {
        $lists = DB::table('pertanyaan')->get();
        // dd($lists);
        return view('pertanyaan.pertanyaan', compact('lists'));
    }

    public function show($id) {
        $sorts = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.show', compact('sorts')); 
    }

    public function edit($id){
        $sorts = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.edit', compact('sorts')); 
    }

    public function update($id, Request $request){
        $query = DB::table('pertanyaan')
                        -> where('id', $id)
                        -> update([
                            "judul" => $request["judul"],
                            "isi" => $request["isi"]
                        ]);
        return redirect('/pertanyaan')->with('success', 'Berhasil update post!');
    }

    public function destroy($id){
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect ('/pertanyaan')->with('success', 'Data berhasil dihapus!');
    }
}   
