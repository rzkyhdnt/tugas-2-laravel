@extends('adminlte.master')

@section('content')
    <div class="card card-primary ml-3 mt-3">
    <div class="card-header">
        <h3 class="card-title">Ajukan Pertanyaan</h3>
    </div>
    <form role="form" action="/pertanyaan" method="POST">
    @csrf
        <div class="card-body">
        <div class="form-group">
            <label for="judul">Judul</label>
            <input type="text" class="form-control" id="judul" placeholder="Masukkan judul" name="judul">
            @error('judul')
                <div class="alert alert-danger">{{ $message="Judul tidak boleh kosong" }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="isi">Isi</label>
            <input type="text   " class="form-control" id="isi" placeholder="Masukkan isi pertanyaan" name="isi">
            @error('isi')
                <div class="alert alert-danger">{{ $message="Isi tidak boleh kosong" }}</div>
            @enderror
        </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
    </div>
@endsection