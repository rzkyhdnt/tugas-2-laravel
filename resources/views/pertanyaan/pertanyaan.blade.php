@extends('adminlte.master')

@section('content')

@if(session('success'))
    <div class="alert alert-success">
        {{session('success')}}
    </div>

@endif
    
<a href="/pertanyaan/create" class="btn btn-primary ml-2 mt-2"> Buat Pertanyaan </a>
    <div class='card ml-2 mt-2'>
        <table class="table table-bordered">
            <thead>                  
                <tr>
                <th style="width: 10px">#</th>
                <th>Judul</th>
                <th>Isi</th>
                <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse($lists as $key => $list)
                <tr>
                    <td> {{$key + 1}} </td>
                    <td> {{$list -> judul}} </td>
                    <td> {{$list -> isi}} </td>
                    <td style="flex">
                        <a href="/pertanyaan/{{$list->id}}" class="btn btn-info btn-sm">Show</a>
                        <a href="/pertanyaan/{{$list->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                        <form action="/pertanyaan/{{$list->id}}" method = "post">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="4" align="center">No Posts</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection